import React from 'react';
import {BrowserRouter, Route} from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min';
import Home from "./pages/Home";
import AmchartsRoute from "./pages/amcharts/AmchartsRoute";

function App() {
    return (
        <BrowserRouter>
            <Route exact path={process.env.REACT_APP_ROUTE_HOME} component={Home} />
            <Route path={process.env.REACT_APP_ROUTE_AMCHARTS} component={AmchartsRoute} />
        </BrowserRouter>
    );
}

export default App;
