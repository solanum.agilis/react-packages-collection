import React, {PureComponent} from 'react';
import {Route} from "react-router-dom";
import AmchartsDefault from "./pages/AmchartsDefault";
import AmchartsMap from "./pages/AmchartsMap";

class AmchartsRoute extends PureComponent {
    render() {
        return (
            <>
                <Route exact path={this.props.match.path} component={AmchartsDefault} />
                <Route path={process.env.REACT_APP_ROUTE_AMCHARTS_MAP} component={AmchartsMap} />
            </>
        );
    }
}

export default AmchartsRoute;