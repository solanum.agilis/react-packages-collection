import React, {PureComponent} from 'react';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4geodata_continentsLow from '@amcharts/amcharts4-geodata/continentsLow';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import data from '../../../data/amcharts/maps.json';

class AmchartsMap extends PureComponent {
    componentDidMount() {
        am4core.ready(() => {
            am4core.useTheme(am4themes_animated);

            let chart = am4core.create('chartdiv', am4maps.MapChart);
            chart.geodata = am4geodata_continentsLow;
            chart.projection = new am4maps.projections.Miller();

            let polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
            polygonSeries.useGeodata = true;
            // polygonSeries.data = data;

            let polygonTemplate = polygonSeries.mapPolygons.template;
            polygonTemplate.tooltipText = "{name}";
            polygonTemplate.fill = am4core.color("#F8F8F8");
            polygonTemplate.stroke = am4core.color("#5CAB7D");
            polygonTemplate.propertyFields.fill = "color";
            polygonTemplate.events.on("hit", event => {
                let country = event.target.dataItem.dataContext;
                console.log('country', country);
                if (typeof country.id !== 'undefined' && country.id !== '' && country.id !== null) {
                    if (country.id === 'asia') {
                        this.props.history.push({
                            pathname: "/"
                        });
                    } else {
                        alert('Not Available in your country!');
                    }
                } else {
                    alert('Not Available in your country!');
                }
            });

            let hs = polygonTemplate.states.create("hover");
            hs.properties.fill = am4core.color("#5CAB7D");

            polygonSeries.exclude = ["antarctica"];

            // chart.backgroundSeries.mapPolygons.template.polygon.fill = am4core.color("#2678BF");
            // chart.backgroundSeries.mapPolygons.template.polygon.fillOpacity = 1;

            chart.zoomControl = new am4maps.ZoomControl();
        });
    }

    render() {
        return (
            <div id="chartdiv" style={{width: '100vw', height: '100vh', backgroundColor: '#2678BF'}}></div>
        );
    }
}

export default AmchartsMap;