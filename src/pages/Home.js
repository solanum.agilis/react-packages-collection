import React, {PureComponent} from 'react';

class Home extends PureComponent {
    render() {
        return (
            <div className="d-flex align-items-center justify-content-center" style={{height: '100vh'}}>
                <h1 className="font-weight-bold">Home</h1>
            </div>
        );
    }
}

export default Home;